extends Control


var _current_selection := ""
var _male_dogsitter = "res://screens/assets/male_dogsitter.png"
var _male_mechanic = "res://screens/assets/male_mechanic.png"
var _male_bartender = "res://screens/assets/male_bartender.png"
var _female_dogsitter = "res://screens/assets/female_dogsitter.png"
var _female_mechanic = "res://screens/assets/female_mechanic.png"
var _female_bartender = "res://screens/assets/female_bartender.png"

onready var _dogsitter = $DogSitterAvatar
onready var _mechanic = $MechanicAvatar
onready var _bartender = $BartenderAvatar
onready var _scene_transition = $SceneTransitionOverlay


func _ready() -> void:
	match PlayerData.avatar.type:
		"male":
			_dogsitter.texture = load(_male_dogsitter)
			_mechanic.texture = load(_male_mechanic)
			_bartender.texture = load(_male_bartender)
		"female":
			_dogsitter.texture = load(_female_dogsitter)
			_mechanic.texture = load(_female_mechanic)
			_bartender.texture = load(_female_bartender)
	
	_current_selection = "dogsitter"


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		PlayerData.job.name = _current_selection
		match PlayerData.avatar.type:
			"male":
				PlayerData.job.file_path = "res://screens/assets/male_avatar_dogsitter.png"
			"female":
				PlayerData.job.file_path = "res://screens/assets/female_avatar_dogsitter.png"
			
		_scene_transition.transition_to(_scene_transition.next_scene_path)
