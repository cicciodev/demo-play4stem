extends Node


var nickname := "Player"
var avatar := {
	"type" : "",
	"main" : "",
	"icon" : ""
}
var job := {
	"name" : "",
	"file_path" : ""
}

var player_stats := {
	"exp_points" : 0,
	"exp_points_total" : 0,
	"reputation_points" : 0,
	"reputation_points_total" : 0,
	"reputation_level" : 0,
	"fallacy_exp_level" : 0,
	"reputation_experience_required" : 0,
	"fallacy_experience_required" : 0
}

var latest_score_id

signal player_stats_changed
signal player_reputation_leveled_up
signal player_fallacy_exp_leveled_up


func _ready() -> void:
	player_stats.reputation_experience_required = _get_required_experience(player_stats.reputation_level + 1)
	player_stats.fallacy_experience_required = _get_required_experience(player_stats.fallacy_exp_level + 1)


func add_reputation_points_to_total():
	_add_points_to_total("reputation_points", "reputation_points_total", "reputation_experience_required",
							"reputation_level", "player_reputation_leveled_up")


func add_exp_points_to_total():
	_add_points_to_total("exp_points", "exp_points_total", "fallacy_experience_required",
							"fallacy_exp_level", "player_fallacy_exp_leveled_up")
	
	_save_player_score(player_stats.exp_points)


func _get_required_experience(level):
	return 10


func _level_up(level : String, signal_name : String, exp_required : String):
	player_stats[level] += 1
	emit_signal(signal_name)
	player_stats[exp_required] = _get_required_experience(player_stats[level] + 1)


func _add_points_to_total(points : String, points_total : String, exp_required : String, level : String, level_signal : String):
	player_stats[points_total] += player_stats[points]
	while player_stats[points] >= player_stats[exp_required]:
		player_stats[points] -= player_stats[exp_required]
		#_reputation_level_up()
		_level_up(level, level_signal, exp_required)
	emit_signal("player_stats_changed")


func _save_player_score(score):
	latest_score_id = yield(SilentWolf.Scores.persist_score(nickname, score), "sw_score_posted")
	print("Score persisted successfully: " + str(latest_score_id))
