extends Control

onready var _user_interface = $UserInterface
onready var _conversation_choices = $ConversationChoices.get_children()
onready var _player_avatar = $PlayerAvatar
onready var _scene_transition = $SceneTransitionOverlay

func _ready() -> void:
	_player_avatar.texture = load(PlayerData.job.file_path)
	for convo in _conversation_choices:
		convo.connect("conversation_set", self, "_on_conversation_set")
	update_interface()


func update_interface():
	var conversations = ConversationData.available_conversation_ids
	var convo_index = 0
	for convo in _conversation_choices:
		convo.update_content(conversations[convo_index])
		
		if _check_user_meets_requirements(conversations[convo_index]) == false:
			convo.disabled = true
		
		convo_index += 1
	
	$ConversationChoices.visible = true
	_user_interface.change_time_of_day()


func _check_user_meets_requirements(conversation_id):
	var conversation_content = ConversationData.get_conversation_content(conversation_id)
	var reputation_needed = conversation_content.reputation_needed
	var exp_needed = conversation_content.exp_needed
	return PlayerData.player_stats.reputation_level >= reputation_needed and PlayerData.player_stats.fallacy_exp_level >= exp_needed


func _on_conversation_set() -> void:
	_scene_transition.transition_to(_scene_transition.next_scene_path)
