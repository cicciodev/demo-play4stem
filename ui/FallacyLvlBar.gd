extends Panel

func _ready() -> void:
	PlayerData.connect("player_stats_changed", self, "update_player_stats")
	update_player_stats()

func update_player_stats():
	$ProgressBar.value = PlayerData.player_stats.exp_points
	$LvlValue.text = "Lv. " + str(PlayerData.player_stats.fallacy_exp_level)
