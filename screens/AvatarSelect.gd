extends Control

var _current_selection := ""
var _male_avatar_ui_icon = "res://ui/assets/avatar_male.png"
var _female_avatar_ui_icon = "res://ui/assets/avatar_female.png"

onready var _male_avatar = $MalePlayer
onready var _female_avatar = $FemalePlayer
onready var _male_avatar_texture = $MalePlayer.texture.resource_path
onready var _female_avatar_texture = $FemalePlayer.texture.resource_path
onready var _scene_transition = $SceneTransitionOverlay


func _ready() -> void:
	_toggle_avatar_trasparency(_male_avatar, _female_avatar)
	_current_selection = "male"


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_right"):
		_highlight_avatar("female")
	if event.is_action_pressed("ui_left"):
		_highlight_avatar("male")
	if event.is_action_pressed("ui_accept"):
		PlayerData.avatar.type = _current_selection
		match _current_selection:
			"male":
				PlayerData.avatar.main = _male_avatar_texture
				PlayerData.avatar.icon = _male_avatar_ui_icon
			"female":
				PlayerData.avatar.main = _female_avatar_texture
				PlayerData.avatar.icon = _female_avatar_ui_icon
		
		_scene_transition.transition_to(_scene_transition.next_scene_path)


func _toggle_avatar_trasparency(highlighted_avatar, other_avatar) -> void:
	highlighted_avatar.modulate.a = 1
	other_avatar.modulate.a = 0.4


func _highlight_avatar(type : String) -> void:
	match type:
		"male":
			_toggle_avatar_trasparency(_male_avatar, _female_avatar)
			_current_selection = type
		"female":
			_toggle_avatar_trasparency(_female_avatar, _male_avatar)
			_current_selection = type


func _on_MalePlayer_mouse_entered() -> void:
	_highlight_avatar("male")


func _on_FemalePlayer_mouse_entered() -> void:
	_highlight_avatar("female")
