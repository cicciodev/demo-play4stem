extends Control


var conversation_file_path : String = ""
var question_start_time : float
var question_elapsed_time : Array = []
var answer_start_time : float
var answer_elapsed_time : Array = []

onready var conversation_handler: ConversationHandler = $ConversationHandler
onready var question_label = $Question/Text
onready var answer_panel = $Answers
onready var answer_buttons = [$Answers/Answer1, $Answers/Answer2,
								$Answers/Answer3, $Answers/Answer4]
onready var _scene_transition = $SceneTransitionOverlay


func _ready() -> void:
	for answer in answer_buttons:
		answer.connect("question_answered", self, "_on_question_answered")
	conversation_handler.connect("content_updated", self, "update_interface")
	conversation_handler.start_conversation()
	update_interface()


func _input(event: InputEvent) -> void:
	if event.is_action_released("ui_accept") and answer_panel.visible == false:
		question_elapsed_time.append({
									"id": conversation_handler.question.id, 
									"time": OS.get_ticks_msec() - question_start_time})
		$Background.modulate = Color("f7d183")
		$AnswerIcons.visible = true
		answer_panel.visible = true
		#_disable_answer_buttons(false)
		answer_start_time = OS.get_ticks_msec()


func update_interface():
	question_label.text = conversation_handler.question.text
	question_start_time = OS.get_ticks_msec()
	
	#_disable_answer_buttons(true)
	var answer_index = 0
	for answer in answer_buttons:
		answer.update_text(conversation_handler.answers[answer_index].text)
		answer_index += 1
	
	$Background.modulate = Color("ffffff")
	$AnswerIcons.visible = false
	answer_panel.visible = false


func reset_start_times():
	question_start_time = 0
	answer_start_time = 0


func _on_question_answered(answer_btn_name):
	var answer_index = int(answer_btn_name.substr(6, 1)) - 1
	answer_elapsed_time.append({
								"id": conversation_handler.answers[answer_index].id, 
								"time": OS.get_ticks_msec() - answer_start_time})
	PlayerData.player_stats.exp_points += int(conversation_handler.answers[answer_index].points)
	reset_start_times()
	
	var next_question_id = conversation_handler.check_next_question_id(answer_index)
	if next_question_id != "":
		conversation_handler.next_question(next_question_id)
	else:
		var convo_content = ConversationData.get_conversation_content(ConversationData.current_conversation_id)
		PlayerData.player_stats.reputation_points += convo_content.reputation_reward
		Global.update_conversation_ids(Global.get_current_conversation_ids(), 
										Global.get_current_time_of_day(), 
										ConversationData.current_conversation_id)
		Global.update_time_of_day()
		_scene_transition.transition_to(_scene_transition.next_scene_path)


func _disable_answer_buttons(state : bool) -> void:
	for answer in answer_buttons:
		answer.disabled = state
