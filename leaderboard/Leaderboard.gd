extends Control

const ScoreItem = preload("ScoreItem.tscn")

var list_index = 0
var ld_name = "main"

onready var _scene_transition = $SceneTransitionOverlay

func _ready() -> void:
	yield(SilentWolf.Scores.get_high_scores(20), "sw_scores_received")
	print("Scores: " + str(SilentWolf.Scores.scores))
	
	for score in SilentWolf.Scores.scores:
		add_item(score.player_name, str(int(score.score)))


func add_item(player_name, score):
	var item = ScoreItem.instance()
	list_index += 1
	item.get_node("PlayerNickname").text = str(list_index) + str(". ") + player_name
	item.get_node("PlayerScore").text = score
	item.margin_top = list_index * 100
	$ScrollContainer/VBoxContainer.add_child(item)


func _on_ButtonClose_button_up() -> void:
	_scene_transition.transition_to(_scene_transition.next_scene_path)
