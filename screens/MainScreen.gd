extends Control


onready var _scene_transition = $SceneTransitionOverlay

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		_scene_transition.transition_to(_scene_transition.next_scene_path)
