extends TextureButton

signal conversation_set

var conversation_file_path: String = ""
var conversation_id: int = 0

var next_scene_path = "res://screens/ConversationIntro.tscn"

func _get_configuration_warning() -> String:
	return "The property Next Level can't be empty" if conversation_file_path == "" else ""

func set_conversation(id):
	conversation_file_path = ConversationData.get_conversation_file_path(id)
	conversation_id = id


func update_content(id) -> void:
	set_conversation(id)
	assert(conversation_id > 0)
	var conversation_content = ConversationData.get_conversation_content(conversation_id)
	$Title.text = conversation_content.title
	if get_node_or_null("Desc") != null:
		$Desc.text = str(conversation_content.desc)
	if get_node_or_null("ReputationRequirementsLbl") != null:
		$ReputationRequirementsLbl.text = "Lv. " + str(conversation_content.reputation_needed)
	if get_node_or_null("ExpRequirementsLbl") != null:
		$ExpRequirementsLbl.text = "Lv. " + str(conversation_content.exp_needed)
	$ReputationRewardsLbl.text = str(conversation_content.reputation_reward) + "pt"
	$ExpRewardsLbl.text = "+" + str(conversation_content.max_exp_reward) + "pt"

func _on_button_up() -> void:
	#PlayerStats.reset()
	assert(conversation_file_path != "")
	assert(conversation_id > 0)
	ConversationData.current_conversation = conversation_file_path
	ConversationData.current_conversation_id = conversation_id
	emit_signal("conversation_set")
	#get_tree().change_scene(next_scene_path)
