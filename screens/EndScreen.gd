extends Control


onready var _user_interface = $UserInterface
onready var _conversation_results = $PickConversationButton
onready var _scene_transition = $SceneTransitionOverlay


func _ready() -> void:
	PlayerData.connect("player_reputation_leveled_up", self, "_on_player_reputation_leveled_up")
	PlayerData.connect("player_fallacy_exp_leveled_up", self, "_on_player_fallacy_exp_leveled_up")
	_user_interface.change_time_of_day()
	
	_conversation_results.update_content(ConversationData.current_conversation_id)
	if _has_player_got_high_score(PlayerData.player_stats.exp_points):
		_conversation_results.get_node("Desc").text = "Hai tenuto testa molto bene al personaggio."
	else:
		_conversation_results.get_node("Desc").text = "Non hai tenuto testa molto bene al personaggio."
	_conversation_results.get_node("ExpRewardsLbl").text = "+ " + str(PlayerData.player_stats.exp_points) + "pt"
	#yield(get_tree().create_timer(1.5), "timeout")
	_conversation_results.visible = true
	#yield(get_tree().create_timer(1.5), "timeout")
	#_conversation_results.visible = false
	
	#PlayerData.add_reputation_points()
	#if $ReputationLevelPopup.visible == true:
		#yield($ReputationLevelPopup, "hide")
	#PlayerData.add_exp_points_to_total()
	#if $FallacyLevelPopup.visible == true:
		#yield($FallacyLevelPopup, "hide")
	
	#if Global.is_match_over:
		#_scene_transition.transition_to("res://leaderboard/Leaderboard.tscn")
	#else:
		#_scene_transition.transition_to(_scene_transition.next_scene_path)


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept") and _conversation_results.visible == true:
		_conversation_results.visible = false
		_update_player_stats()


func _has_player_got_high_score(score):
	var conversation_content = ConversationData.get_conversation_content(ConversationData.current_conversation_id)
	if score >= (conversation_content.max_exp_reward / 2):
		return true
	return false


func _show_results():
	_conversation_results.update_content(ConversationData.current_conversation_id)
	_conversation_results.get_node("Desc").text = "Text"
	_conversation_results.get_node("Rewards/ExpReward/Label").text = str(PlayerData.player_stats.exp_points)
	yield(get_tree().create_timer(1.5), "timeout")
	_conversation_results.visible = true
	yield(get_tree().create_timer(1.5), "timeout")
	_conversation_results.visible = false


func _update_player_stats():
	PlayerData.add_reputation_points_to_total()
	if $ReputationLevelPopup.visible == true:
		yield($ReputationLevelPopup, "hide")
	PlayerData.add_exp_points_to_total()
	if $FallacyLevelPopup.visible == true:
		yield($FallacyLevelPopup, "hide")
	
	_go_to_next_scene()


func _go_to_next_scene():
	if Global.is_match_over:
		_scene_transition.transition_to("res://leaderboard/Leaderboard.tscn")
	else:
		_scene_transition.transition_to(_scene_transition.next_scene_path)


func _on_player_reputation_leveled_up():
	$Background.texture = load("res://screens/assets/background_match_end.png")
	$Background.modulate = Color("ffffff")
	var popup_text = "Sei riuscito a raggiungere il Lvl. " + str(PlayerData.player_stats.reputation_level) + ", molto bene!"
	$ReputationLevelPopup/Box.texture = load("res://ui/assets/level_up_box.png")
	$ReputationLevelPopup.show_level_popup(popup_text)


func _on_player_fallacy_exp_leveled_up():
	$Background.texture = load("res://screens/assets/background_match_end.png")
	$Background.modulate = Color("ffffff")
	var popup_text = "Sei riuscito a raggiungere il Lvl. " + str(PlayerData.player_stats.fallacy_exp_level) + " in punti dialogo, molto bene!"
	$FallacyLevelPopup/Box.texture = load("res://ui/assets/exp_level_up_box.png")
	$FallacyLevelPopup.show_level_popup(popup_text)
