extends TextureButton

signal question_answered(btn_name)

func update_text(text):
	$Label.text = text

func _on_button_up() -> void:
	emit_signal("question_answered", self.name)
