extends Node


enum TimesOfDay {
	MORNING,
	AFTERNOON,
	EVENING
}

const NUMBER_OF_DAYS := 4
const MINIMUM_NUMBER_OF_CONVERSATIONS := 3

signal time_of_day_changed
signal day_changed

var _morning_conversations_ids := [1, 2, 3, 4]
var _afternoon_conversations_ids := [5, 6, 7, 8]
var _evening_conversations_ids := [9, 10, 11, 12]
var _current_day_index := 1
var _current_time_of_day = TimesOfDay.MORNING
var is_match_over := false

var api_key : String


func _ready() -> void:
	var f = File.new()
	f.open('res://api_key.env', File.READ)
	api_key = f.get_line()
	f.close()

	SilentWolf.configure({
			"api_key": self.api_key,
			"game_id": "equery",
			"game_version": "1.0.0",
			"log_level": 1
	})
	
	SilentWolf.configure_scores({
			"open_scene_on_close": "res://menus/ConversationMenu.tscn"
	})
	
	yield(SilentWolf.Scores.get_high_scores(0), "sw_scores_received")


func get_current_time_of_day():
	var keys = TimesOfDay.keys()
	return keys[_current_time_of_day].capitalize()


func get_current_conversation_ids():
	if _current_time_of_day == TimesOfDay.MORNING:
		return _morning_conversations_ids
	if _current_time_of_day == TimesOfDay.AFTERNOON:
		return _afternoon_conversations_ids
	if _current_time_of_day == TimesOfDay.EVENING:
		return _evening_conversations_ids


func update_conversation_ids(ids : Array, time_of_day : String, cleared_convo_id : int) -> void:
	if ids.size() > MINIMUM_NUMBER_OF_CONVERSATIONS:
		var index = ids.find(cleared_convo_id)
		ids.remove(index)
		
		if time_of_day == TimesOfDay.keys()[TimesOfDay.EVENING].capitalize():
			_evening_conversations_ids = ids
		elif time_of_day == TimesOfDay.keys()[TimesOfDay.AFTERNOON].capitalize():
			_afternoon_conversations_ids = ids
		elif time_of_day == TimesOfDay.keys()[TimesOfDay.MORNING].capitalize():
			_morning_conversations_ids = ids

func update_time_of_day():
	if _current_time_of_day == TimesOfDay.EVENING:
		_current_time_of_day = TimesOfDay.MORNING
		_update_day()
	elif _current_time_of_day == TimesOfDay.AFTERNOON:
		_current_time_of_day = TimesOfDay.EVENING
	elif _current_time_of_day == TimesOfDay.MORNING:
		_current_time_of_day = TimesOfDay.AFTERNOON
	
	emit_signal("time_of_day_changed")


func _update_day():
	if _current_day_index < NUMBER_OF_DAYS:
		_current_day_index += 1
	else:
		is_match_over = true
