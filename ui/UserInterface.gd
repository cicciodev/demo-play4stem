extends Control

onready var _player_icon = $PlayerInfo/Icon
onready var _player_nickname = $PlayerInfo/Nickname
onready var _time_of_day_nodes = [$Morning, $Afternoon, $Evening]


func _ready() -> void:
	_player_icon.texture = load(PlayerData.avatar.icon)
	_player_nickname.text = PlayerData.nickname


func change_time_of_day():
	var time = Global.get_current_time_of_day()
	for time_node in _time_of_day_nodes:
		if time_node.name == time:
			time_node.visible = true
			time_node.rect_position.x = 1639
			time_node.rect_position.y = 0
		else:
			time_node.visible = false
