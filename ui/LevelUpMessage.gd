extends Panel


onready var _anim_player = $AnimationPlayer


func _ready() -> void:
	print("i'm in the ready function of the level up message " + str(self.name))
	#set_process_input(false)


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		print("i'm in the input function of the level up message " + str(self.name))
		_anim_player.play_backwards("appear")
		yield(_anim_player, "animation_finished")
		visible = false
		#set_process_input(false)
		get_tree().paused = false


func show_level_popup(text):
	print("i'm in the show level popup function of the level up message " + str(self.name))
	#set_process_input(true)
	$ColorRect/Message.text = text
	visible = true
	_anim_player.play("appear")
	yield(_anim_player, "animation_finished")
	get_tree().paused = true 
