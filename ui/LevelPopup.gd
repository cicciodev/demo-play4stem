extends Popup


func _ready() -> void:
	set_process_input(false)


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		hide()
		set_process_input(false)
		get_tree().paused = false


func show_level_popup(text):
	set_process_input(true)
	$Box/Label.text = text
	popup_centered()
	get_tree().paused = true 
